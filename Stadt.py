from Klasse_Lager import Lager, stock, zubehoer
from Warentausch import *
from Schiff import schifftypen, neuesSchiff
import math
import random

class Stadt():
    def __init__(self, name, einwohner, produktionsgut, entwicklungsstufe, koordinaten):
        self.name = name
        self.einwohner = einwohner
        self.produktionsgueter = produktionsgut
        self.level = entwicklungsstufe
        self.standort = koordinaten
        
        self.konsumProKopf = {'Nahrung': 0.00125, 'Sonstiges': 0.002}
        self.stadtverbrauch = self.verbrauch()

        self.markt = Markt(self.stadtverbrauch)
        if self.level < 2:
            self.werft = Werft(handel=False)
        else:
            self.werft = Werft()

    def verbrauch(self):
        stadtkonsum = {}
        for i in self.konsumProKopf:
            stadtkonsum[i] = self.konsumProKopf.get(i)*self.einwohner
        return stadtkonsum

    def verbrauchen(self):
        for key in self.stadtverbrauch:
            for ware in stock():
                if ware.kategorie == key:
                    menge = self.stadtverbrauch.get(key)
                    if self.markt.lager.warenmenge(ware) == None:
                        break
                    else:
                        self.markt.lager.warenAusladen(ware, self.stadtverbrauch.get(key))
        self.einwohner += round(self.einwohner*self.konsumProKopf.get('Nahrung')*25.5)
        self.stadtverbrauch = self.verbrauch()
        
    def produzieren(self):
        for gut in self.produktionsgueter:
            self.markt.lager.warenEinladen(gut, random.randint(10, round(self.einwohner/25)))

    def stadtinfo(self):
        print('Stadt: {}'.format(self.name))
        print('Einwohner: {}'.format(self.einwohner))
        print('Verbrauch: ')
        for kategorie in self.stadtverbrauch.keys():
            print('    {} : {}'.format(kategorie, self.stadtverbrauch.get(kategorie)))
        print('--------------------')
        self.markt.lager.ladungAnzeigen()
              
    def __str__(self):
        return '{}[{} Einw.] liegt bei x={}, y={}'.format(self.name, self.einwohner, self.x_pos, self.y_pos)
        

class Markt():
    def __init__(self, bedarf):
        self.lager = Lager()
        self.bedarf = bedarf

        for artikel in self.lager.waren.produkte:
            if artikel.kategorie == 'Nahrung':
                self.lager.warenEinladen(artikel.name, round(self.bedarf.get('Nahrung') + random.randint(0, 50)))
            if artikel.kategorie == 'Sonstiges':
                self.lager.warenEinladen(artikel.name, round(self.bedarf.get('Sonstiges') + random.randint(10, 80)))


        self.lager.frachtEinladen('Kanone', random.randint(10, 30))
        self.lager.frachtEinladen('Entermesser', random.randint(22, 65))
        self.lager.frachtEinladen('Kanonenkugel', random.randint(10, 25))

    def lagerAnzeigen(self):
        waren_kaufpreis = self.warenKaufpreis()
        waren_verkaufpreis = self.warenVerkaufpreis()
        fracht_kaufpreis = self.frachtKaufpreis()
        fracht_verkaufpreis = self.frachtVerkaufpreis()
        print('Waren:')
        for artikel in self.lager.waren.produkte:
            if self.lager.warenmenge(artikel.name) != None:
                artikelmenge = round(self.lager.warenmenge(artikel.name))
            else:
                artikelmenge = 0
            print('{}t {} - vk: {}$ | k: {}$'.format(artikelmenge, artikel.name,
                                                     waren_verkaufpreis.get(artikel.name),
                                                     waren_kaufpreis.get(artikel.name)))
        print('--------------------')
        print('Fracht:')
        for artikel in self.lager.fracht.produkte:
            if self.lager.frachtmenge(artikel.name) != None:
                artikelmenge = round(self.lager.frachtmenge(artikel.name))
            else:
                artikelmenge = 0
            print('{} {} - vk: {}$ | k: {}$'.format(artikelmenge, artikel.name,
                                                    fracht_verkaufpreis.get(artikel.name),
                                                    fracht_kaufpreis.get(artikel.name)))

    def ermittelPreis(self, option, name):
        for artikel in stock()+zubehoer():
            if name == artikel.name:
                grundpreis = artikel.preis
                verbrauch = self.bedarf.get(artikel.kategorie)
                menge = self.lager.warenmenge(artikel.name)
                if menge == None:
                    menge = self.lager.frachtmenge(artikel.name)
                if option == 'verkaufen':
                    if menge == None:
                        menge = 0
                    if verbrauch == None:
                        verbrauch = 0
                    return round((grundpreis + (verbrauch*50)**math.exp(-menge/120)),1)
                else:
                    if menge == None:
                        menge = 0
                    if verbrauch == None:
                        verbrauch = 0
                    return round((grundpreis + (verbrauch*50)**math.exp(-menge/150)),1)
            
    def warenKaufpreis(self):
        kpreis = {}
        for artikel in self.lager.waren.produkte:
            if artikel.kategorie == 'Nahrung':
                kpreis[artikel.name] = self.ermittelPreis('kaufen', artikel.name)
            if artikel.kategorie == 'Sonstiges':
                kpreis[artikel.name] = self.ermittelPreis('kaufen', artikel.name)
        return kpreis
    def warenVerkaufpreis(self):
        vkpreis = {}
        for artikel in self.lager.waren.produkte:
            if artikel.kategorie == 'Nahrung':
                vkpreis[artikel.name] = self.ermittelPreis('verkaufen', artikel.name)
            if artikel.kategorie == 'Sonstiges':
                vkpreis[artikel.name] = self.ermittelPreis('verkaufen', artikel.name)
        return vkpreis
    
    def warenKaufen(self, kundenlager, name, menge):
        if name in self.lager.waren.ladung:
            if menge > self.lager.warenmenge(name):
                menge = self.lager.warenmenge(name)
            preis = self.warenKaufpreis().get(name) * menge
            warenVonNach(self.lager, kundenlager, name, menge)
            return preis
        else:
            return False
    def warenVerkaufen(self, kundenlager, name, menge):
        if name in kundenlager.lager.waren.ladung:
            if menge > kundenlager.lager.warenmenge(name):
                menge = kundenlager.lager.warenmenge(name)
            preis = self.warenVerkaufpreis().get(name) * menge
            warenVonNach(kundenlager, self.lager, name, menge)
            return preis
        else:
            return False

    def frachtKaufpreis(self):
        kpreis = {}
        for artikel in self.lager.fracht.produkte:
            kpreis[artikel.name] = artikel.preis
        return kpreis
    def frachtVerkaufpreis(self):
        vkpreis = {}
        for artikel in self.lager.fracht.produkte:
            vkpreis[artikel.name] = artikel.preis
        return vkpreis
    
    def frachtKaufen(self, kundenlager, name, menge):
        if name in self.lager.fracht.ladung:
            if menge > self.lager.frachtmenge(name):
                menge = self.lager.frachtmenge(name)
            preis = self.frachtKaufpreis().get(name) * menge
            frachtVonNach(self.lager, kundenlager, name, menge)
            return preis
        else:
            return False
    def frachtVerkaufen(self, kundenlager, name, menge):
        if name in kundenlager.lager.fracht.ladung:
            if menge > kundenlager.lager.frachtmenge(name):
                menge = kundenlager.lager.frachtmenge(name)
            preis = self.frachtVerkaufpreis().get(name) * menge
            frachtVonNach(kundenlager, self.lager, name, menge)
            return preis
        else:
            return False

    def __str__(self):
        return ' '

class Werft():
    def __init__(self, handel = True):
        self.handelsberechtigung = handel

    def schiffnamenliste(self):
        liste = []
        for schiff in schifftypen():
            liste.append(schiff[0])
        return liste

    def schiffpreis(self, name):
        return neuesSchiff(name).preis

    def reparationskosten(self, benutzer):
        max_hp = 0
        for schiff in user.konvoi:
            max_hp += schiff.max_hp

class Taverne():
    def __init__(self):
        pass
