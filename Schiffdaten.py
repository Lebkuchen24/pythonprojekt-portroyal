SCHALUPPE = {'laderaum':180, 'matrosen':60, 'kanonen':12, 'hp':100, 'maxGeschwindigkeit':11, 'wendigkeit':100}
BRIGG = {'laderaum':230, 'matrosen':70, 'kanonen':14, 'hp':110, 'maxGeschwindigkeit':11, 'wendigkeit':95}
BARKE = {'laderaum':230, 'matrosen':90, 'kanonen':18, 'hp':140, 'maxGeschwindigkeit':12, 'wendigkeit':90}
KORVETTE = {'laderaum':330, 'matrosen':110, 'kanonen':22, 'hp':200, 'maxGeschwindigkeit':12, 'wendigkeit':85}
FLEUTE = {'laderaum':480, 'matrosen':70, 'kanonen':14, 'hp':220, 'maxGeschwindigkeit':10, 'wendigkeit':85}
FREGATTE = {'laderaum':380, 'matrosen':120, 'kanonen':24, 'hp':220, 'maxGeschwindigkeit':11, 'wendigkeit':80}
GALEONE = {'laderaum':580, 'matrosen':170, 'kanonen':34, 'hp':280, 'maxGeschwindigkeit':10, 'wendigkeit':75}
KARAVELLE = {'laderaum':480, 'matrosen':190, 'kanonen':38, 'hp':300, 'maxGeschwindigkeit':11, 'wendigkeit':75}
LINIENSCHIFF = {'laderaum':380, 'matrosen':230, 'kanonen':48, 'hp':340, 'maxGeschwindigkeit':14, 'wendigkeit':70}


SCHIFFE = {'Schaluppe':SCHALUPPE, 'Brigg':BRIGG, 'Barke':BARKE, 'Korvette':KORVETTE, 'Fleute':FLEUTE, 'Fregatte':FREGATTE, 'Galeone':GALEONE, 'Karavelle':KARAVELLE, 'Linienschiff':LINIENSCHIFF}

#for i in SCHIFFE.keys():
#    if SCHIFFE.get(i).get('hp') <= 200 :
#        print('Verfuegbare Schiffe:%s' %(i))
