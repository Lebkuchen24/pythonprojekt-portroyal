class Benutzer():
    def __init__(self, name):
        self.name = name
        self.heimat = ''
        self.standort = [0, 0]
        self.geld = 0
        self.konvoi = []

    def neuerStandort(self, x, y):
        self.standort[0] = x
        self.standort[1] = y
        
    def plusGeld(self, betrag):
        self.geld += betrag
    def minusGeld(self, betrag):
        self.geld -= betrag

    def plusSchiff(self, schiff):
        self.konvoi.append(schiff)
    def minusSchiff(self, listenposition):
        del self.konvoi[listenposition]
    
