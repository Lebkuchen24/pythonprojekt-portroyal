# Es bietet sich die Möglichkeit an Stelle von lager ein anderes schiff zu uebergeben!
def warenVonNach(lager1, lager2, name, menge):
    lademenge = lager2.warenEinladen(name, menge)
    lager1.warenAusladen(name, lademenge)

def frachtVonNach(lager1, lager2, name, menge):
    lademenge = lager2.frachtEinladen(name, menge)
    lager1.frachtAusladen(name, lademenge)
