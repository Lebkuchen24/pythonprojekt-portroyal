class Produkt():
    def __init__(self, name, kategorie ,preis, gewicht=1.0):
        self.name = name
        self.kategorie = kategorie
        self.preis = preis
        self.gewicht = gewicht

    def __str__(self):
        return '{}$ pro {}t {}'.format(self.preis, self.gewicht, self.name)



class Produktverwaltung():
    def __init__(self, produktliste):
        self.ladung = {}
        self.produkte = produktliste
        self.gewicht = 0.0

    def einladen(self, name, menge):

        if name in self.ladung:
            self.ladung[name] = self.ladung.get(name) + menge
            # Gewicht neu berechnen
            self.gewichtNeuBerechnen(name, menge, '+')
        
        else:
            for produkt in self.produkte:
                if name == produkt.name:
                    self.ladung[produkt.name] = menge
                    # Gewicht neu berechnen
                    self.gewicht += (produkt.gewicht * menge)

    def ausladen(self, name, menge):

        if name in self.ladung:
            ladungsBestand = self.ladung.get(name)
            if menge > ladungsBestand:
                print('Du hast vom Produkt "{}" nur {}t'.format(name, ladungsBestand))
                
            elif menge == ladungsBestand:
                del self.ladung[name]
                # Gewicht neu berechnen
                self.gewichtNeuBerechnen(name, menge, '-')

            else:
                self.ladung[name] = self.ladung.get(name) - menge
                # Gewicht neu berechnen
                self.gewichtNeuBerechnen(name, menge, '-')
                
        else:
            print('Du hast kein Produkt "{}"'.format(name))
            
    def gewichtNeuBerechnen(self, name, menge, operator):
        for produkt in self.produkte:
            if name == produkt.name:
                if operator == '-':
                    self.gewicht -= (produkt.gewicht * menge)
                else:
                    self.gewicht += (produkt.gewicht * menge)



class Lager():
    def __init__(self, max_ladungskapazitaet=False):
        self.max_ladungskapazitaet = max_ladungskapazitaet
        self.waren = Produktverwaltung(stock())
        self.fracht = Produktverwaltung(zubehoer())
        self.nutz_ladungskapazitaet = 0.0

    def warenmenge(self, name):
        return self.waren.ladung.get(name)
    def frachtmenge(self, name):
        return self.fracht.ladung.get(name)
    
    # Gibt den Laderaum wieder
    def ladungAnzeigen(self):
        print('Waren:')
        for produkt in self.waren.ladung:
            print('{}t - {}'.format(round(self.waren.ladung.get(produkt),1), produkt))
        print('--------------------')
        print('Fracht:')
        for produkt in self.fracht.ladung:
            print('{} - {}'.format(round(self.fracht.ladung.get(produkt),1), produkt))
        if self.max_ladungskapazitaet != False:
            print('--------------------')
            print('Genutzte Ladungskapazitaet: {}t von {}t'.format(round(self.nutz_ladungskapazitaet + 0.4), self.max_ladungskapazitaet))


    # Waren ein-/ausladen
    def warenEinladen(self, name, menge):
        '''Gibt bei zu hohen Mengen den entsprechenden Rest zurück. ansonsten ist rest = 0'''
        if self.max_ladungskapazitaet != False:
            # Kontrolle ob zuviel eingeladen wurde. Und wenn Ja, dann soviel einladen wie möglich
            for produkt in self.waren.produkte:
                if name == produkt.name:  
                    if (produkt.gewicht * menge) + self.nutz_ladungskapazitaet > self.max_ladungskapazitaet:
                        menge = int((self.max_ladungskapazitaet - self.nutz_ladungskapazitaet) / produkt.gewicht)
        
            self.waren.einladen(name, menge)
            self.nutz_ladungskapazitaet = self.berechneKapazitaet()
        else:
            self.waren.einladen(name, menge)
        return menge

    def warenAusladen(self, name, menge):
        self.waren.ausladen(name, menge)
        if self.max_ladungskapazitaet != False:
            self.nutz_ladungskapazitaet = self.berechneKapazitaet()


    # Fracht ein-/ausladen    
    def frachtEinladen(self, name, menge):
        '''Gibt bei zu hohen Mengen den entsprechenden Rest zurück. ansonsten ist rest = 0'''
        if self.max_ladungskapazitaet != False:
            # Kontrolle ob zuviel eingeladen wurde. Und wenn Ja, dann soviel einladen wie möglich
            for produkt in self.fracht.produkte:
                if name == produkt.name:
                    if (produkt.gewicht * menge) + self.nutz_ladungskapazitaet > self.max_ladungskapazitaet:
                        menge = int((self.max_ladungskapazitaet - self.nutz_ladungskapazitaet) / produkt.gewicht)
                    
            self.fracht.einladen(name, menge)
            self.nutz_ladungskapazitaet = self.berechneKapazitaet()
        else:
            self.fracht.einladen(name, menge)
        return menge

    def frachtAusladen(self, name, menge):
        self.fracht.ausladen(name, menge)
        if self.max_ladungskapazitaet != False:
            self.nutz_ladungskapazitaet = self.berechneKapazitaet()


    # Berechnung der Kapazitaet aus Fracht und Waren
    def berechneKapazitaet(self):
        kapazitaet = self.waren.gewicht + self.fracht.gewicht
        return kapazitaet


# warenliste = '''Getreide,Kartoffel,Fisch,Fleisch,
# Holz,Salz,Zucker,Baumwolle,Gewuerze'''.split(',')
def stock():
    stock = []
    artikelpreise = [['Getreide', 'Nahrung', 40], ['Fisch', 'Nahrung', 50],
                     ['Kartoffel', 'Nahrung', 40],['Fleisch', 'Nahrung', 50],
                     ['Holz', 'Sonstiges', 40], ['Salz', 'Sonstiges', 60],
                     ['Zucker', 'Sonstiges', 80], ['Baumwolle', 'Sonstiges', 80],
                     ['Gewuerze', 'Sonstiges', 100]]
    for artikel in artikelpreise:
        stock.append(Produkt(artikel[0], artikel[1], artikel[2]))
    return stock
        

def zubehoer():
    zubehoer = []
    zubehoer.append(Produkt('Kanone', 'Ausstattung', 280, 3.0))
    zubehoer.append(Produkt('Entermesser', 'Ausstattung', 35, 0.0015))
    zubehoer.append(Produkt('Kanonenkugel', 'Ausstattung', 6, 0.015))
    return zubehoer
