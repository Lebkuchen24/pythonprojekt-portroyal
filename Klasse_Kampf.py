from Klasse_Schiff import neuesSchiff

import random

def summeMatrosen(konvoi):
    matrosen = 0
    for i in konvoi:
        matrosen += i.matrosen
    return matrosen
    
def summeKanonen(konvoi):
    kanonen = 0
    for i in konvoi:
        kanonen += i.lager.fracht.ladung.get('Kanone')
    return kanonen

def summeMunition(konvoi):
    munition = 0
    for i in konvoi:
        munition += i.lager.fracht.ladung.get('Kanonenkugel')
    return munition

def summeHP(konvoi):
    hp = 0
    for i in konvoi:
        hp += i.hp
    return hp

class Kampf():
    def __init__(self, angreiferKonvoi, verteidigerKonvoi):
        self.aggressor = angreiferKonvoi
        self.verteidiger = verteidigerKonvoi
        self.kugelschaden = 1.2

    def automatisch(self, modus='versenken'):
        angreifer_matrosen = summeMatrosen(self.aggressor)
        angreifer_kanonen = summeKanonen(self.aggressor)
        angreifer_munition = summeMunition(self.aggressor)
        angreifer_hp = summeHP(self.aggressor)

        verteidiger_matrosen = summeMatrosen(self.verteidiger)
        verteidiger_kanonen = summeKanonen(self.verteidiger)
        verteidiger_munition = summeMunition(self.verteidiger)
        verteidiger_hp = summeHP(self.verteidiger)

        if modus == 'versenken':
            spieler = random.randint(0,1)
            while True:
                if spieler == 0:
                    print('Spieler: Angreifer')
                    schaden = (0.5 * angreifer_kanonen)* self.kugelschaden
                    verteidiger_hp -= schaden
                    print('hat {} Schaden angerichtet - {}'.format(schaden, verteidiger_hp))
                    input()
                    if verteidiger_hp <= 0:
                        print('Angreifer hat gewonnen')
                        break
                    spieler = 1

                if spieler == 1:
                    print('Spieler: Verteidiger')
                    schaden = (0.5 * verteidiger_kanonen) * self.kugelschaden
                    angreifer_hp -= schaden
                    print('hat {} Schaden angerichtet - {}'.format(schaden, angreifer_hp))
                    input()
                    if angreifer_hp <= 0:
                        print('Verteidiger hat gewonnen')
                        break
                    spieler = 0


