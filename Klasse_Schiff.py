from Klasse_Lager import Lager

class Schiff():
    def __init__(self, typ, laderaum, matrosen, kanonen, hp, knoten, wendigkeit):
        self.schifftyp = typ
        self.laderaum = laderaum
        self.max_matrosen = matrosen
        self.max_kanonen = kanonen
        self.max_hp = hp
        self.max_knoten = knoten
        self.wendigkeit = wendigkeit
        
        self.lager = Lager(self.laderaum)
        self.matrosen = 0
        self.hp = self.max_hp


    def warenEinladen(self, name, menge):
        return self.lager.warenEinladen(name, menge)
    def warenAusladen(self, name, menge):
        return self.lager.warenAusladen(name, menge)

    def frachtEinladen(self, name, menge):
        bereits_geladen = self.lager.fracht.ladung.get(name)
        if name == 'Kanone':
            if bereits_geladen == None:
                if menge > self.max_kanonen:
                    menge = self.max_kanonen - bereits_geladen
            else:
                if bereits_geladen + menge > self.max_kanonen:
                    menge = self.max_kanonen - bereits_geladen
            self.lager.frachtEinladen(name, menge)
            return menge
        else:
            return self.lager.frachtEinladen(name, menge)
    def frachtAusladen(self, name, menge):
        return self.lager.frachtAusladen(name, menge)

        
    def addiereMatrosen(self, anzahl):
        if self.matrosen + anzahl <= self.max_matrosen:
            self.matrosen += anzahl
    def subtrahiereMatrosen(self, anzahl):
        if self.matrosen - anzahl >= 0:
            self.matrosen -= anzahl
        
    def __str__(self):
        return('{}({}t) mit {} Matrosen hat max. {} Kanonen - HP: {} - Knoten: {} - Wendigkeit: {}%'
              .format(self.schifftyp, self.laderaum, self.max_matrosen, self.max_kanonen,
                      self.max_hp, self.max_knoten, self.wendigkeit))

def schifftypen():
    liste = []
    liste.append(('Schaluppe', 180, 60, 12, 100, 11, 100))
    liste.append(('Brigg', 230, 70, 14, 110, 11, 95))
    liste.append(('Barke', 230, 90, 18, 140, 12, 90))
    liste.append(('Korvette', 330, 110, 22, 200, 12, 85))
    liste.append(('Fleute', 480, 70, 14, 220, 10, 85))
    liste.append(('Fregatte', 380, 120, 24, 220, 11, 80))
    liste.append(('Galeone', 580, 170, 34, 280, 10, 75))
    liste.append(('Karavelle', 480, 190, 38, 300, 11, 75))
    liste.append(('Linienschiff', 380, 230, 48, 340, 14, 70))
    return liste

def neuesSchiff(typ):
    st = schifftypen()
    nummer = 0
    for name in st:
        if name[0] != typ:
            nummer += 1
        else:
            return Schiff(st[nummer][0], st[nummer][1], st[nummer][2], st[nummer][3],
                  st[nummer][4], st[nummer][5], st[nummer][6])
