from Stadt import Stadt

nahrung = ['Getreide', 'Kartoffel', 'Fisch', 'Fleisch']
luxus = ['Salz', 'Zucker', 'Gewuerze']
plantage = ['Holz', 'Baumwolle']
stadtwerte = [['Ekbeiw', 1200, plantage, 5, [50, 0]],
              ['Ika', 600, luxus, 2, [20, 100]],
              ['Ibel', 280, nahrung, 1, [100, 50]]]
staedte = []
for i in stadtwerte:
    staedte.append(Stadt(i[0], i[1], i[2], i[3], i[4]))


from Schiff import neuesSchiff
#schiffe = [neuesSchiff('Karavelle'), neuesSchiff('Fregatte'), neuesSchiff('Schaluppe')]

import math
from datetime import datetime
from Start import text
from Benutzer import *
from Warentausch import *

def stadtSetup(stadt, user):
    def stadtverlassen():
        while True:
            print('willst du wirklich die Stadt verlassen? (j/n)')
            eingabe = input().lower()
            if eingabe.startswith('n'):
                return False
            if eingabe.startswith('j'):
                return True

    print()        
    print('# Wilkommen in {} #'.format(stadt.name))
    print('Einwohner: {} | '.format(stadt.einwohner))
    while True:
        print('Was ist dein Anliegen (Markt/Werft/Taverne/Verwaltung)?')
        eingabe = input()
        eingabe.lower()
        if eingabe == 'markt':
            marktSetup(stadt, user)
        if eingabe == 'werft':
            werftSetup(stadt, user)
        if eingabe == 'taverne':
            taverneSetup(stadt, user)
        if eingabe == 'verwaltung':
            verwaltungSetup(stadt, user)
        if eingabebearbeitung(eingabe, user) and stadtverlassen():
            if user.konvoi[0].matrosen >= 8:
                break
            else:
                print('Du hast zuwenig Matrosen zum Auslaufen.')

def marktSetup(stadt, user):
    def kaufen(stadt, user):
        kundenlager = user.konvoi[0]
        while True:
            print('Welchen Artikel?')
            artikel_eingabe = input()
            print('Und wieviel?')
            menge_eingabe = input()
            if menge_eingabe.isdigit():
                menge_eingabe = int(menge_eingabe)
                marktpreis = stadt.markt.ermittelPreis('kaufen', artikel_eingabe)
                if marktpreis != None:
                    if user.geld < marktpreis*menge_eingabe:
                        menge_eingabe = int(user.geld/marktpreis)
                    waren_kosten = stadt.markt.warenKaufen(kundenlager, artikel_eingabe, menge_eingabe)
                    if waren_kosten != False:
                        return waren_kosten
                    fracht_kosten = stadt.markt.frachtKaufen(kundenlager, artikel_eingabe, menge_eingabe)
                    if fracht_kosten != False:
                        return fracht_kosten
                else:
                    return 0

    def verkaufen(stadt, user):
        kundenlager = user.konvoi[0]
        while True:
            print('Welchen Artikel?')
            artikel_eingabe = input()
            print('Und wieviel?')
            menge_eingabe = input()
            if menge_eingabe.isdigit():
                menge_eingabe = int(menge_eingabe)
                waren_kosten = stadt.markt.warenVerkaufen(kundenlager, artikel_eingabe, menge_eingabe)
                if waren_kosten != False:
                    return waren_kosten
                fracht_kosten = stadt.markt.frachtVerkaufen(kundenlager, artikel_eingabe, menge_eingabe)
                if fracht_kosten != False:
                    return fracht_kosten
    print()
    print('# Der Hafen von {} #'.format(stadt.name))
    while True:
        stadt.markt.lagerAnzeigen()
        print()
        print('kaufen oder verkaufen(k/vk)?')
        eingabe = input().lower()
        if eingabe == 'k' or eingabe == 'kaufen':
            user.minusGeld(kaufen(stadt, user))
        if eingabe == 'vk' or eingabe == 'verkaufen':
            user.plusGeld(verkaufen(stadt, user))
        if eingabebearbeitung(eingabe, user):
            break

def werftSetup(stadt, user):
    def kaufen(eingabe, user):
        preis = stadt.werft.schiffpreis(eingabe)
        user.plusSchiff(neuesSchiff(eingabe))

        matrosen = user.konvoi[0].matrosen
        user.konvoi[1].matrosen = matrosen
        waren = user.konvoi[0].lager.waren.ladung
        warennamen = []
        for i in waren:
            warennamen.append(i)
        for i in warennamen:
            warenVonNach(user.konvoi[0], user.konvoi[1], i, user.konvoi[0].lager.warenmenge(i))

        fracht = user.konvoi[0].lager.fracht.ladung
        frachtnamen = []
        for i in fracht:
            frachtnamen.append(i)
        for i in frachtnamen:
            frachtVonNach(user.konvoi[0], user.konvoi[1], i, user.konvoi[0].lager.frachtmenge(i))

        user.minusSchiff(0)
        
            
        return preis
    print()
    print('# Die Werft von {} #'.format(stadt.name))
    if stadt.werft.handelsberechtigung == True:
        while True:
            print('Schiff kaufen? (kaufen/verkaufen/reparieren)')
            eingabe = input().lower()
            if eingabe == 'k' or eingabe == 'kaufen':
                while True:
                    schiffnamenliste = stadt.werft.schiffnamenliste()
                    for name in schiffnamenliste:
                        print(name, end=', ')
                    print()
                    eingabe = input()
                    if eingabe in schiffnamenliste:
                        user.minusGeld(kaufen(eingabe, user))
                        break
            if eingabe == 'vk' or eingabe == 'verkaufen':
                print('Kein Verkauf von Schiffen momentan.')
            if eingabe == 'r' or eingabe == 'reparieren':
                print('Reparatur nicht notwendig, da dein Schiff wie neu ist.')
            if eingabebearbeitung(eingabe, user):
                break
    else:
        print('! Momentan nicht möglich, auf Grund der Stadtentwicklung !')

def taverneSetup(stadt, user):
    print()
    print('# Die Taverne von {} #'.format(stadt.name))
    while True:
        print('Wonach suchst du? (Matrosen, Quest, Kapitaen)')
        eingabe = input().lower()
        if eingabe == 'matrosen':
            while True:
                print('Wieviele brauchst du?')
                eingabe = input()
                if eingabe.isdigit():
                    user.konvoi[0].addiereMatrosen(int(eingabe))
                    break
        if eingabe == 'quest':
            print('Quests sind momentan nicht verfügbar.')
        if eingabe == 'kapitaen':
            print('Kein Kapitaen in der Taverne')
        if eingabebearbeitung(eingabe, user):
            break

def verwaltungSetup(stadt, user):
    print()
    print('# Die Verwaltung von {} #'.format(stadt.name))
    while True:
        print('(Speicher/Gebaeudekauf/Kaperbrief/Blackboard)')
        eingabe = input().lower()
        if eingabe == 'speicher':
            print('Du besitzt keinen Speicher in dieser Stadt.')
        if eingabe == 'gebaeudekauf':
            print('Dir fehlt die Genehmigung.')
        if eingabe == 'kaperbrief':
            print('Wir sind nicht im Kriege mit Anderen.')
        if eingabe == 'blackboard':
            print('Keine Piraten unterwegs.')
        if eingabebearbeitung(eingabe, user):
            break
            
def seefahrtSetup(staedte, user):
    beenden = False
    while not beenden:
        print()
        print('Welchen Hafen willst du anfahren?')
        for stadt in staedte:
            print('{} {}'.format(stadt.name, stadt.standort),end=', ')
        print()
        eingabe = input()
        for stadt in staedte:
            if eingabe.lower() == stadt.name.lower():
                alte_Position = user.standort
                neue_Position = stadt.standort
                
                x = math.fabs(alte_Position[0] - neue_Position[0])
                y = math.fabs(alte_Position[1] - neue_Position[1])
                zielentfernung = round(math.sqrt((x**2) + (y**2))) # zielentfernung in km
                schiffgesch = (user.konvoi[0].max_knoten * 1.85) # schiffgesch in km/h
                reisezeit = zielentfernung/schiffgesch # reisezeit in stunden
                
                user.neuerStandort(stadt.standort[0], stadt.standort[1])
                beenden = True
                print('reisezeit:', end = '')
                print(reisezeit)
                break
    return reisezeit # in stunden

def eingabebearbeitung(eingabe, user):
    if eingabe.startswith('l'):
        return True
    if eingabe.startswith('i'):
        print()
        print(datum.strftime('- %A %d. %B - %Y -'))
        print('Spieler {} kommt aus {}'.format(user.name, user.heimat))
        print('Guthaben: {}$ | Schifftyp: {} | Matrosen: {}'.format(user.geld, user.konvoi[0].schifftyp, user.konvoi[0].matrosen))
        for i in user.konvoi:
            i.lager.ladungAnzeigen()
        print()

            
        

# Programmablauf:
print(text)
while True:    
    print('Wie heist du (max. 10 Zeichen)?')
    eingabe = input()
    if len(eingabe) <= 10:
        user = Benutzer(eingabe)
        break
    
print('Wie moechtest du starten?')
while True:
    print('Mit einer Schaluppe oder einer Brigg?')
    eingabe = input().lower()
    if eingabe == 'schaluppe':
        user.plusSchiff(neuesSchiff('Schaluppe'))
        user.plusGeld(5000)
        break
    if eingabe == 'brigg':
        user.plusSchiff(neuesSchiff('Brigg'))
        user.plusGeld(350000)
        #user.konvoi[0].addiereMatrosen(50)
        break
    
user.heimat = staedte[0].name
user.neuerStandort(staedte[0].standort[0], staedte[0].standort[1])
datum = datetime(1860, 1, 1)
global datum
while True:    
    for stadt in staedte:
        if user.standort == stadt.standort:
            stadtSetup(stadt, user)
            reisezeitInStunden = seefahrtSetup(staedte, user)
                
            reisezeitInStunden += datum.minute/60
            #print('reisezeit in Stunden += datum.minute/60 -> ', end='')
            #print(reisezeitInStunden)
            reisezeitInMinuten = (reisezeitInStunden - int(reisezeitInStunden))*60
            #print('reisezeitInMinuten = (reisezeitInStunden - int(reisezeitInStunden))*60 ->', end = '')
            #print(reisezeitInMinuten)
            reisezeitInStunden = datum.hour + int(reisezeitInStunden)
            #print('reisezeitInStunden = datum.hour + int(reisezeitInStunden) -> ', end = '')
            #print(reisezeitInStunden)
            
            datum = datum.replace(minute = round(reisezeitInMinuten))
            if reisezeitInStunden >= 24:
                #print('reisezeitInStunden >= 24')
                reisezeitInTagen = int(reisezeitInStunden/24)
                reisezeitInStunden = reisezeitInStunden - (reisezeitInTagen*24)
                datum = datum.replace(hour = reisezeitInStunden)
                for i in range(reisezeitInTagen):
                    for stadt in staedte:
                        stadt.verbrauchen()
                        stadt.produzieren()
                    try:
                        datum = datum.replace(day = datum.day+1)
                    except ValueError:
                        try:
                            datum = datum.replace(month = datum.month+1, day = 1)
                        except ValueError:
                            datum = datum.replace(year = datum.year+1, month = 1, day = 1)
            else:
                #print('reisezeitInStunden < 24')
                datum = datum.replace(hour = reisezeitInStunden)
            break
